﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson_8
{
    class Knife:Weapon
    {
        private string knife;

        public Knife()
        {
            knife = "Нож";
            Damage = -10;
            Range = 1;
        }

        public override void ShowWeapon()
        {
            base.ShowWeapon();
            Console.WriteLine(knife);
            Console.WriteLine("Урон от атаки" + Damage);
            Console.WriteLine("Номер оружия" + Range);
        }
    }
}
