﻿using System;

namespace Lesson_8
{
    class Program
    {
        static void Main(string[] args)
        {
            Players player_1 = new Player_1();
            Players player_2 = new Player_2();
 
           
            player_1.ShowInfo("Игрок_1", 100);
            player_1.SelectWeapon();

            
            player_2.ShowInfo("Игрок_2", 100);
            player_2.SelectWeapon();
        }
    }
}
