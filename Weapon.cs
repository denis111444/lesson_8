﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson_8
{
    class Weapon
    {
        protected int Damage;
        protected int Range;
        public void SetDmage(int Damage)
        {
            this.Damage = Damage;
        }

        public void SetRange(int Range)
        {
            this.Range = Range;
        }

        public virtual void ShowWeapon()
        {
            Console.WriteLine("Выбранное оружие:");
        }    
    }
}
