﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson_8
{
    class Players
    {
        protected Weapon weapon = new Knife();
        protected Weapon weapon1 = new Pistol();

        
        protected string Name;      
        protected int Health;
        

        public virtual void SelectWeapon()
        {
            
            Console.WriteLine("Выберите тип оружия:\nНажммите цифру 1 для выбора Ножа\nНажммите цифру 2 для выбора Пистолет");
            int Select = Convert.ToInt32(Console.ReadLine());

            if (Select == 1)
            {
                weapon.ShowWeapon();

            }

            if (Select == 2)
            {
                weapon1.ShowWeapon();
            }
        }

        public virtual void ShowInfo(string Name, int Health )
        {
           
            this.Name = Name;
            this.Health = Health;          

            
            Console.WriteLine("Имя игрока:" + Name);
            Console.WriteLine("Здоровье игрока:" + Health);
        }



    }
}
