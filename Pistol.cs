﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson_8
{
    class Pistol:Weapon
    {
        private string pistol;

        public Pistol()
        {
            pistol = "Пистолет";
            Damage = -30;
            Range = 2;
        }

        public override void ShowWeapon()
        {
            base.ShowWeapon();
            Console.WriteLine(pistol);
            Console.WriteLine("Урон от атаки" + Damage);
            Console.WriteLine("Номер оружия" + Range);
        }
    }
}
